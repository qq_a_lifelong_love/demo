package com.sjh.demo.entity;



import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @Author: sjh
 * @Date: 2023-07-27
 * @Version: 1.0
 */

public class UserEntity implements Serializable {

    private Long id;
    private String guid;
    private String name;
    private String age;
    private Date createTime;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(guid, that.guid) && Objects.equals(name, that.name) && Objects.equals(age, that.age) && Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, guid, name, age, createTime);
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public UserEntity() {
    }

    public UserEntity(Long id, String guid, String name, String age, Date createTime) {
        this.id = id;
        this.guid = guid;
        this.name = name;
        this.age = age;
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}