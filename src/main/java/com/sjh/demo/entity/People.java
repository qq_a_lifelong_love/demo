package com.sjh.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: sjh
 * @Date: 2023-08-27
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class People {

    private String name;

    private Integer age;

    private boolean isChinese;
}
