package com.sjh.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-22
 * @Version: 1.0
 */
public class ListAndSet {
    //    ArrayList集合中如何删除重复的元素
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("a");
        list.add("c");
        list.add("b");

//  思路一：创建新集合，然后遍历老集合，看当前遍历到的元素是否在新集合中存在，存在不添加，不存在就添加
//        历完之后，新集合中存的就是不重复元素
        List<String> newList = new ArrayList<>();
        for (String str : list) {
            if (!newList.contains(str)) {
                newList.add(str);
            }
        }
        System.out.println(newList);

        /**
         * 思路二：通过Set集合（无序，唯一）实现
         * 创建set集合，吧list集合中的元素添加到set集合中，清空list集合，吧set集合中（去掉重复的元素）放到list中
         */
        HashSet hashSet = new HashSet();
        hashSet.addAll(list);
        list.clear();
        list.addAll(hashSet);
        System.out.println(list);
    }
}
