package com.sjh.demo.ListAndStream;

import com.sjh.demo.entity.People;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: sjh
 * @Date: 2023-08-27
 * @Version: 1.0
 */
//@SpringBootTest
public class Stream {

    @Test
    public void testLambox() {
//        1.根据对象中的某个字段进行相关操作
        List<String> list = new ArrayList<>();
        list.add("xxx2023001");
        list.add("xxx2023002");
        list.add("xxx2023003");
        List<String> collect = list.stream().filter(s -> s.contains("002")).collect(Collectors.toList());
        System.out.println("过滤出包含002的列表字符串元素" + collect);


//    2根据对象中的某个字段进行相关操作
        List<People> peopleList = new ArrayList<>();
        People people1 = new People();
        people1.setName("小王");
        people1.setAge(30);
        people1.setChinese(true);
        People people2 = new People("Tim", 28, false);
        People people3 = new People("jack", 29, false);
        peopleList.add(people1);
        peopleList.add(people2);
        peopleList.add(people3);
//        列表根据某个字段进行排序(根据年龄进行排序)
        List<People> people = peopleList.stream().sorted(Comparator.comparing(People::getAge)).collect(Collectors.toList());
        people.forEach(System.out::println);

        List<People> peoples = peopleList.stream().sorted(Comparator.comparing(People::getName)).collect(Collectors.toList());
        peoples.forEach(System.out::println);
//        根据某个属性分组，key为对应属性值
        Map<Boolean, List<People>> peopleMap = peopleList.stream().collect(Collectors.groupingBy(People::isChinese));
//        获取名字和年龄的映射关系（key value）
        Map<String, Integer> nameAgeMap = peopleList.stream().collect(Collectors.toMap(People::getName, People::getAge));

//        嵌套使用
        List<List<People>> peopleLists = new ArrayList<>();
        peopleLists.add(peopleList);

        peopleLists.forEach(System.out::println);

        List<People> peopleList2 = new ArrayList<>();
        peopleList2.add(new People("小王2",25,true));
        peopleList2.add(new People("Tim2",21,false));
        peopleList2.add(new People("jack",30,false));
        peopleLists.add(peopleList2);

//        平铺获取嵌套结构某个属性
        List<String> allNames = peopleLists.stream()
                .flatMap(r -> r.stream().map(People::getName)).collect(Collectors.toList());
        System.out.println("0");
        allNames.forEach(System.out::println);

//        根据某个属性对嵌套结构排序并打平为列表返回
        List<People> allSortedAges = peopleLists.stream()
                .flatMap(Collection::stream).sorted(Comparator.comparing(People::getAge)).collect(Collectors.toList());
        System.out.println("1");
        allSortedAges.forEach(System.out::println);

    }

}
