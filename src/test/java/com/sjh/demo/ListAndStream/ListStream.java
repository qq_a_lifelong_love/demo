package com.sjh.demo.ListAndStream;

import com.sjh.demo.entity.People;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: sjh
 * @Date: 2023-08-13
 * @Version: 1.0
 */
public class ListStream {
    public static void main(String[] args) {
        List<Integer> userList = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < 1000; i++) {
            userList.add(rand.nextInt(100));

        }
        List<Integer> userList2 = new ArrayList<>();
        userList2.addAll(userList);

        long startTime1 = System.currentTimeMillis();
        userList2.stream().sorted(Comparator.comparing(Integer::intValue)).collect(Collectors.toList());

        long endTime1 = System.currentTimeMillis();
        System.out.println("stream.sort耗时:" + (endTime1 - startTime1) + "ms");

        long startTime2 = System.currentTimeMillis();
        userList.sort(Comparator.comparing(Integer::intValue));
        long endTime2 = System.currentTimeMillis();
        System.out.println("List.sort()耗时:" + (endTime2 - startTime2 + "ms"));
    }

    @Test
    public void test1() {

        List<Integer> userList = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < 1000; i++) {
            userList.add(rand.nextInt(100));

        }
        List<Integer> userList2 = new ArrayList<>();
        userList2.addAll(userList);

        long startTime2 = System.currentTimeMillis();
        userList.sort(Comparator.comparing(Integer::intValue));
        long endTime2 = System.currentTimeMillis();
        System.out.println("List.sort()耗时:" + (endTime2 - startTime2 + "ms"));

        long startTime1 = System.currentTimeMillis();
        userList2.stream().sorted(Comparator.comparing(Integer::intValue)).collect(Collectors.toList());
        long endTime1 = System.currentTimeMillis();
        System.out.println("stream.sort耗时:" + (endTime1 - startTime1) + "ms");

    }


}
