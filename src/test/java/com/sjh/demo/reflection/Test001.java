package com.sjh.demo.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * @Author: 宋军辉
 * @Date: 2024-06-02
 * @Version: 1.0
 *
 * 通过反射越过泛型检查
 *
 * 例如：有一个String的泛型集合，怎么能想这个歌集合中添加一个Integer类型的值
 */
public class Test001 {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");

//        list.add(100);
//        获取ArrayList的class对象，反向的调用add（）方法，添加数据
        Class<? extends ArrayList> listClass = list.getClass();
//        获取add方法
        Method add = listClass.getMethod("add", Object.class);
//        调用add（）方法
        add.invoke(list,100);

//        遍历集合
        for (Object obj : list) {
            System.out.println(obj);
        }

    }
}
