package com.sjh.demo.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Author: 宋军辉
 * @Date: 2024-06-02
 * @Version: 1.0
 */
public class Stu {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        MyUtils utils = new MyUtils();
        Class<? extends MyUtils> aClass = utils.getClass();
        Method[] methods = aClass.getDeclaredMethods();
        for (Method method : methods) {
            method.invoke(utils);
        }
    }
}
