package com.sjh.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @Author: sjh
 * @Date: 2023-07-26
 * @Version: 1.0
 */
//@SpringBootTest
public class test {

  /*  @Test
    public void test05() {

        int cardStart;
        int cardEnd;
        int i = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("next");
        String s = scanner.next();
//        for (i = cardStart; i < cardEnd; i++) {
//            String s = "";
        if (i >= 0 && i <= 9) {
            s = "0000" + String.valueOf(i);
        } else if (i >= 10 && i <= 99) {
            s = "000" + String.valueOf(i);
        } else if (i >= 100 && i <= 999) {
            s = "00" + String.valueOf(i);
        } else if (i >= 1000 && i <= 9999) {
            s = "0" + String.valueOf(i);
        }
        System.out.println(s);
    }*/

    @Test
    public void test001() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("a");
        list.add("d");

        List<String> newList = new ArrayList<>();
        for (String str : list) {
            if (!newList.contains(str)) {
                newList.add(str);
            }

        }
        System.out.println(newList);


        HashSet<String> hashSet = new HashSet<>();
        hashSet.addAll(list);
        list.clear();
        list.addAll(hashSet);
        System.out.println(list);
    }
}
