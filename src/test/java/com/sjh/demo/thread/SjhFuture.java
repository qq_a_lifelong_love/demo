package com.sjh.demo.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * .
 * @Author: sjh
 * @Date: 2023-08-06
 * @Version: 1.0
 */
public class SjhFuture {
    public static void main(String[] args) {
        FutureTask<Object> task = new FutureTask<>(new SjhThread3());
        new Thread(task).start();
        Class<? extends FutureTask> aClass = task.getClass();
        System.out.println(aClass);


        new Thread(task).start();
    }
}
