package com.sjh.demo.thread;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: sjh
 * @Date: 2023-08-10
 * @Version: 1.0
 */
public class ThreadTest {

//    public static Object lock = new Object();
    public static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
//            执行业务逻辑
//            synchronized (lock){
//                for (int i = 0; i < 100; i++) {
//                    System.out.println(i);
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
            lock.lock();
            for (int i = 0; i < 100; i++) {
                System.out.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            lock.unlock();
        });
        t1.start();
        Thread.sleep(3000);
//        调用stop()方法就直接停掉线程，不建议使用
//        t1.stop();
//        interrupt()方法使用来中断线程
        t1.interrupt();
//        synchronized (lock){
//            System.out.println("end");
//        }
        lock.lock();
        System.out.println("over end");
        lock.unlock();
    }
}
