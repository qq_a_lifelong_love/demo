package com.sjh.demo.thread;

/**
 * @Author: sjh
 * @Date: 2023-08-29
 * @Version: 1.0
 */
public class VolatileStopThread implements Runnable {

    private volatile boolean canceled = false;

    @Override
    public void run() {
        int num = 0;
        try {
            while (!canceled && num <= 100000) {
                if (num % 10 == 0) {
                    System.out.println(num + "是10的倍数");
                }
                num ++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            System.out.println(num);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        VolatileStopThread r = new VolatileStopThread();
        r.run();
        Thread thread = new Thread();
        thread.start();
        Thread.sleep(30000);
        r.canceled = true;
    }
}
