package com.sjh.demo.thread;

/**
 * @Author: sjh
 * @Date: 2023-08-06
 * @Version: 1.0
 */

/**
 * 实现Runnable接口
 * 通过实现Runnable接口，并实现run方法，也可以创建一个线程
 * 首先定义一个类实现Runnable接口，，并实现run方法
 * 然后创建Runnable实现类对象，并把它作为target传入Thread的构造函数中
 * 最后调用start方法启动线程
 */
public class SjhThread2 implements Runnable{
    @Override
    public void run() {
        System.out.println("第一个线程 Thread 。。。");
    }

    public static void main(String[] args) {
        new Thread(new SjhThread2()).start();
    }
}
