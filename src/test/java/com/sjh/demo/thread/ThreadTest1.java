package com.sjh.demo.thread;

/**
 * @Author: sjh
 * @Date: 2023-08-10
 * @Version: 1.0
 */
public class ThreadTest1 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(()->{
            for (int i = 0; i < 1000000; i++) {
//                判断当前线程有没有被中断,和i>50 时,才能被中断掉
                if (Thread.currentThread().isInterrupted()&&i>300000){
//                    若被中断，则跳出循环
                    break;
                }
                System.out.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        t1.start();
        Thread.sleep(1000);
        t1.interrupt();
        System.out.println("end");
    }
}
