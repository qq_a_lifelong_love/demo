package com.sjh.demo.thread;

/**
 * @Author: sjh
 * @Date: 2023-08-29
 * @Version: 1.0
 */
//使用interrupt停止一个线程

/**
 * 首先判断判断贤臣是否被中断
 * 然后判断count是否小于1000
 * 这个线程的工作内容很简单，就是打印0-999的数字，每次打印一个数字count加1，可以看到，
 * 线程会在每次循环开始之前，定期是否被中断了
 */
public class StopThread implements Runnable {
    @Override
    public void run() {
        int count = 0;
        while (!Thread.currentThread().isInterrupted() && count < 1000) {
            System.out.println("count = " + count++);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new StopThread());
        thread.start();

        
        Thread.sleep(50000);
        thread.interrupt();//
    }

    private void reInterrupt(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();//调用中止线程
            e.printStackTrace();
        }
    }
}
