package com.sjh.demo.thread;

/**
 * @Author: sjh
 * @Date: 2023-08-06
 * @Version: 1.0
 */

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 使用jdk自带的Executors来创建线程池对象
 * 首先，定义一个Runnable的实现类，重写run方法
 * 然后创建一个固定线程线程数的线程池
 * 最后通过ExecutorService对象的execute方法传入线程对象
 */
public class SjhThread4 implements Runnable {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "    Thread run ...");
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            executor.execute(new SjhThread4());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        停止接收新任务，原来的任务继续执行
        executor.shutdown();
    }
}
