package com.sjh.demo.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @Author: sjh
 * @Date: 2023-08-06
 * @Version: 1.0
 */

/**
 * 实现Calllable接口并结合future实现
 * 首先定义一个Callable的实现类，并实现Callable方法，call方法是带返回值的
 * 然后通过FutureTask的构造方法，把这个Callable实现类传递进去
 * 把FutureTask作为Thread类的target，创建Thread实现类传进去
 * 通过FutureTask的get方法获取线程的执行结果
 */

public class SjhThread3 implements Callable<Object> {

    @Override
    public Object call() throws Exception {
        return "hello callable";
    }
}
