package com.sjh.demo.thread;

/**
 * @Author: sjh
 * @Date: 2023-08-06
 * @Version: 1.0
 */

/**
 * 1.继承Thread类
 * 继承Thread类并重写run方法，可以先创建一个线程
 * 首先定义一个类来继承Thread类，重写run方法
 * 然后创建这个子类对象，并调用start方法启动线程
 */
public class SjhThread1 extends Thread{
    @Override
    public void run(){
        System.out.println("第一个线程 Thread 。。。");
    }

    public static void main(String[] args) {
        new SjhThread1().start();
    }
}
