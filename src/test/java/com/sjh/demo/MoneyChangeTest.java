package com.sjh.demo;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Author: sjh
 * @Date: 2023-08-02
 * @Version: 1.0
 */
public class MoneyChangeTest {
    public static void main(String[] args) {
//        找零金额
        int amount = 17;
//        纸币面值
        int[] allMoney = {20, 10, 5, 1};
        Map<Integer, Integer> map = changeMoney(allMoney, amount);
//        map.entrySet()  遍历map的一种方式
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "元" + entry.getValue() + "个");
        }
    }

    private static Map<Integer, Integer> changeMoney(int[] allMoney, int amount) {
        if (amount<=0||allMoney==null||allMoney.length==0){
            throw new IllegalArgumentException("无效的金额");
        }
//        Comparator.reverseOrder()倒叙排序发   1.8的新特性
        Map<Integer, Integer> map = new TreeMap<>(Comparator.reverseOrder());
        for (int money : allMoney) {
            int num = amount / money;

            if (num > 0) {
                map.put(money, num);
                amount %= money;
            }
        }
        return map;
    }
}
