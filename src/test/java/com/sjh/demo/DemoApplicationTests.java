package com.sjh.demo;

import com.sjh.demo.entity.Student;
import com.sjh.demo.service.ClassService;
import org.assertj.core.util.DateUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

@SpringBootTest
class DemoApplicationTests {
    @Autowired
    private ClassService classService;

    @Test
    void contextLoads() {
        /*List<Student> allStudent = classService.getAllStudent();
        allStudent.forEach(System.out::println);*/
       /* if (isValidMobileNumber())
        boolean isValidMobileNumber(String s){
            // 是否是11位？
            if (s.length() != 11) {
                return false;
            } // 每一位都是0~9：
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);

                if (c < '0' || c > '9') {
                    return false;
                }
            }
            return true;
        }*/

    }


    @Test
    public void test1() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("当前时间：" + now);
//        int hour = now.getHour();
//        System.out.println("当前的小时："+hour);
        LocalDateTime localDateTime2 = now.plusYears(1);
        System.out.println("当前时间加一年" + localDateTime2);

        LocalDateTime localDateTime3 = now.plusMonths(1);
        System.out.println("当前时间加一月" + localDateTime3);

        LocalDateTime dateTime = now.plusDays(1);
        System.out.println("当前时间加一天：" + dateTime);

        LocalDateTime localDateTime = now.plusHours(1);
        System.out.println("当前时间加一个小时" + localDateTime);

        LocalDateTime localDateTime1 = now.plusMinutes(5);
        System.out.println("当前时间加5分钟" + localDateTime1);

        LocalDateTime localDateTime4 = now.plusSeconds(20);
        System.out.println("当前时间加20秒" + localDateTime4);

        LocalDateTime localDateTime5 = now.plusWeeks(1);
        System.out.println("当前时间加一周" + localDateTime5);

        LocalDateTime localDateTime6 = now.plusNanos(900000000);
        System.out.println("当前时间加多少纳秒" + localDateTime6);

//        ==============
        int dayOfYear = now.getDayOfYear();
        System.out.println("一年的第多少天?" + dayOfYear);

        int dayOfMonth = now.getDayOfMonth();
        System.out.println("一月的第多少天：" + dayOfMonth);

        DayOfWeek dayOfWeek = now.getDayOfWeek();
        System.out.println("星期几：" + dayOfWeek);

        int year = now.getYear();
        Month month = now.getMonth();

        int dayOfMonth1 = now.getDayOfMonth();

        int hour = now.getHour();
        int minute = now.getMinute();
        int second = now.getSecond();
        System.out.println("年:" + year + "  月：" + month + "  日：" + dayOfMonth1 + "  小时" + hour + "   分钟：" + minute + "   秒：" + second);
    }


    @Test
    public void test02() {
        String s = "0009876543210";
        String s1 = s.substring(0, 5);//只要前5位
        System.out.println(s1);

        String s2 = s.substring(3);//截掉前三位，只要后边的
        System.out.println(s2);

        String s3 = s.replaceFirst("^0", "");//将第一个0置换成 空
        System.out.println(s3);

        String s4 = s.replace("0", "1");//将字符串中的0全都换成1
        System.out.println(s4);
    }

    @Test
    public void test03() {
        StringBuilder sb = new StringBuilder(10);
        sb.append("me");
        System.out.println(sb);

        sb.append("!");
        System.out.println(sb);

        sb.insert(2, "java");
        System.out.println(sb);

        sb.insert(4, 5);
        System.out.println(sb);

        sb.delete(4, 6);//删除第四到第六
        System.out.println(sb);
    }

    @Test
    public void test04() {
        String context = "I am noob" + "from runoob.com";
        System.out.println(context);
        String pattern = "runoob";


        boolean isMatch = Pattern.matches(pattern, context);
        System.out.println("字符串中是否包含了‘runoob’字符串？" + isMatch);

        String s1 = "abc";            // 常量池
        String s2 = new String("abc");     // 堆内存中
        System.out.println(s1 == s2);        // false两个对象的地址值不一样。
        System.out.println(s1.equals(s2)); // true

        String s3 = "a" + "b" + "c";
        String s4 = "abc";
        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));

        String s5 = "ab";
        String s6 = "abc";
        String s7 = s5 + "c";
        System.out.println(s7 == s6);         // false
        System.out.println(s7.equals(s6));  // true
    }




}
