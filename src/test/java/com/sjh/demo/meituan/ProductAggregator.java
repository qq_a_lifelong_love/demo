package com.sjh.demo.meituan;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-22
 * @Version: 1.0
 */
public class ProductAggregator {

    /**
     * 目标：现在有一个List<Product>。我们期望list中productId都是不重复的。但是因为上游操作的问题，传给我们的list中有部分productId相同的数据，我们需要先对他们进行聚合。
     * 例如list中有2个元素(productId:1,price:5,num:10),(productId:1,price:5,num:3),则需要聚合成(productId:1,price:5,num:13) 聚合后，
     * 根据总价进行从大到小的排序，总价一样的情况下根据单价从大到小排序。
     * 请写一个函数，入参是原始的List<Product>，出参是最后排序好的集合。
     */
    @Test
    public void test() {

       /* Product product1 = new Product(1, 5.0, 10);
        Product product2 = new Product(1, 5.0, 3);
        Product product3 = new Product(2, 56.0, 5);
        Product product4 = new Product(3, 4.5, 20);

        List<Product> products=new ArrayList();
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);*/
        List<Product> products = Arrays.asList(
                new Product(1, 5.0, 10),
                new Product(1, 5.0, 3),
                new Product(2, 56.0, 5),
                new Product(3, 4.5, 20)
        );


        List<Product> sortedAggregatedProducts = aggregateAndSort(products);
        // 打印结果
        sortedAggregatedProducts.forEach(System.out::println);
    }

    public static List<Product> aggregateAndSort(List<Product> products) {
        // 使用Map进行聚合
        Map<Integer, Product> aggregatedProducts = new HashMap<>();
        for (Product product : products) {
            int productId = product.getProductId();
            if (aggregatedProducts.containsKey(productId)) {
                Product existingProduct = aggregatedProducts.get(productId);
                existingProduct.setNum(existingProduct.getNum() + product.getNum());
            } else {
                aggregatedProducts.put(productId, product);
            }
        }

        // 将Map的values转换为List
        List<Product> aggregatedList = new ArrayList<>(aggregatedProducts.values());

        // 根据总价和单价进行排序
        aggregatedList.sort((p1, p2) -> {
            int totalPriceDiff = Double.compare(p2.getTotalPrice(), p1.getTotalPrice());
            if (totalPriceDiff != 0.0) {
                return totalPriceDiff;
            } else {
                return Double.compare(p2.getPrice(), p1.getPrice());
            }
        });
        return aggregatedList;
    }


    class Product {
        private int productId;
        private double price;
        private int num;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Product)) return false;
            Product product = (Product) o;
            return productId == product.productId && Double.compare(product.price, price) == 0 && num == product.num;
        }

        @Override
        public int hashCode() {
            return Objects.hash(productId, price, num);
        }

        public Product() {
        }

        public Product(int productId, double price, int num) {
            this.productId = productId;
            this.price = price;
            this.num = num;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

//    toString方法进行打印


        @Override
        public String toString() {
            return "Product{" +
                    "productId=" + productId +
                    ", price=" + price +
                    ", num=" + num +
                    '}';
        }

        //    计算总价
        public double getTotalPrice() {
            return price * num;
        }
    }

}

