package com.sjh.demo.dataTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/**
 * @Author: sjh
 * @Date: 2023-09-12
 * @Version: 1.0
 */
public class DateTimeFormatterChange {


    public static void main(String[] args) {
//        LocalDate parse = LocalDate.parse("2020-02-02", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//        System.out.println(parse);
//        LocalDate yyyyMMdd = LocalDate.parse("2020-02-02", DateTimeFormatter.ofPattern("yyyyMMdd"));
//        System.out.println(yyyyMMdd);
//        LocalDate parse1 = LocalDate.parse("2020-02-02", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
//        System.out.println(parse1);
//        LocalDate parse2 = LocalDate.parse("2020-02-02", DateTimeFormatter.ofPattern("HH:mm:ss"));
//        System.out.println(parse2);

        //创建日期时间对象格式化器，日期格式类似： 2020-02-23 22:18:38
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        //时间格式字符串
        String sDate="2020-02-23 22:18:38";
        //将时间格式字符串转化为LocalDateTime对象，需传入日期对象格式化器
        LocalDateTime parseDate=LocalDateTime.parse(sDate,formatter);
        System.out.println(parseDate);


        LocalDate localDate = LocalDate.now();
        String format = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate);
        System.out.println(format);
//        LocalDate yyyyMMdd = LocalDate.parse(format, DateTimeFormatter.ofPattern("yyyyMMdd"));
//        System.out.println(yyyyMMdd);


        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("MMM dd, yyyy");
        String formattedDate1 = formatter1.format(localDate);
        System.out.println(formattedDate1); //Dec 17, 2018

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MMM dd, yyyy", Locale.CANADA);
        String formattedDate2 = formatter2.format(localDate);
        System.out.println(formattedDate2); //Dec. 17, 2018

        DateTimeFormatter formatter3 = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        String formattedDate3 = formatter3.format(localDate);
        System.out.println(formattedDate3); //Monday, December 17, 2018

        LocalDateTime localDateTime = LocalDateTime.now();

        DateTimeFormatter formatter4 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        String formattedDate4 = formatter4.format(localDateTime);
        System.out.println(formattedDate4); //Dec 17, 2018, 9:14:39 PM

        DateTimeFormatter formatter5 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT);
        String formattedDate5 = formatter5.format(localDateTime);
        System.out.println(formattedDate5); //December 17, 2018, 9:14 PM

        LocalTime localTime = LocalTime.now();

        DateTimeFormatter formatter6 = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM);
        String formattedDate6 = formatter6.format(localTime);
        System.out.println(formattedDate6); //9:14:39 PM
    }

}
