package com.sjh.demo.dataTime;

import org.junit.jupiter.api.Test;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author: sjh
 * @Date: 2023-08-21
 * @Version: 1.0
 */

public class LocalDateTime_eg {
    public static void main(String[] args) {
//        获取标准时间可以通过System.currentTimeMillis()方法获取，此方法不受时区影响，得到的结果是时间戳格式的
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        String format = dateFormat.format(date);
        System.out.println(format);
    }

    //在 Java 中，获取当前日期最简单的方法之一就是直接实例化位于 Java 包 java.util 的 Date 类
    @Test
    public void testDate(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String format = dateFormat.format(date);
        System.out.println(format);
    }

    //Calendar 类，专门用于转换特定时刻和日历字段之间的日期和时间
    //与 date 一样，我们也可以非常轻松地 format 这个日期成我们需要的格式
    @Test
    public void testCalendarAPI(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String format = dateFormat.format(calendar.getTime());
        System.out.println(format);
    }

    // LocalDate 只是一个日期，没有时间。 这意味着我们只能获得当前日期，但没有一天的具体时间
    @Test
    public void testLocalDate(){
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String format = date.format(formatter);
        System.out.println("format ="+format);
        System.out.println("date ="+date);
    }

    //LocalTime 与 LocalDate 相反，它只代表一个时间，没有日期。 这意味着我们只能获得当天的当前时间，而不是实际日期
    @Test
    public void testLocalTime(){
        LocalTime time = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String format = time.format(formatter);
        System.out.println("format ="+format);
        System.out.println("time ="+time);
    }

    //LocalDateTime，也是 Java 中最常用的 Date / Time 类，代表前两个类的组合 – 即日期和时间的值
    @Test
    public void testLocalDateTime(){
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM-dd HH:mm:ss");
        String format = dateTime.format(formatter);
        System.out.println("format ="+format);

    }

}
