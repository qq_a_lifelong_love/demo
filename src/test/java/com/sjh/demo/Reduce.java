package com.sjh.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

/**
 * @Author: sjh
 * @Date: 2023-07-31
 * @Version: 1.0
 */
@SpringBootTest
public class Reduce {

    /**
     * T reduce(T identity, BinaryOperator<T> accumulator);
     * identity：它允许用户提供一个循环计算的初始值。
     * accumulator：计算的累加器，
     */

    @Test
    public  void testReduce(){

        //T reduce(T identity, BinaryOperator<T> accumulator);
        System.out.println("给定个初始值，求和");
        System.out.println(Stream.of(1,2,3,4).reduce(100,(sum,item)->sum+item));
        System.out.println(Stream.of(1,2,3,4).reduce(100,Integer::sum));

        System.out.println("给定个初始值，求min");
        System.out.println(Stream.of(1,2,3,4).reduce(100,(min,item)->Math.min(min,item)));
        System.out.println(Stream.of(1,2,3,4).reduce(100,Integer::min));

        System.out.println("给定初始值，求max");
        System.out.println(Stream.of(1,2,3,4).reduce(100,(max,item)->Math.max(max,item)));
        System.out.println(Stream.of(1,2,3,4).reduce(100,Integer::max));

        //Optional<T> reduce(BinaryOperator<T> accumulator);
        // 注意返回值，上面的返回是T,泛型，传进去啥类型，返回就是啥类型。
        // 下面的返回的则是Optional类型
        System.out.println("无初始值，进行求和");
        System.out.println(Stream.of(1,2,3,4).reduce(Integer::sum).orElse(0));
        System.out.println("无初始值，求max");
        System.out.println(Stream.of(1,2,3,4).reduce(Integer::max).orElse(0));
        System.out.println("无初始值，求min");
        System.out.println(Stream.of(1,2,3,4).reduce(Integer::min).orElse(0));


    }
}
