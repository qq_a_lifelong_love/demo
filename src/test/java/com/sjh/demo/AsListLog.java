package com.sjh.demo;


import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;



/**
 * @Author: sjh
 * @Date: 2023-08-04
 * @Version: 1.0
 */

@SpringBootTest
public class AsListLog {

    public static void main(String[] args) {
        List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        IntSummaryStatistics status = primes.stream().mapToInt((x) -> x).summaryStatistics();
//        logger.info("Highest prime number in List:" + status.getMax());
//        logger.info("Sum of number in List:" + status.getMin());
//        logger.info("Average of All numbers:" + status.getAverage());
//        logger.info("count of all prime number:" + status.getCount());
        System.out.println(status.getMax());
    }
}
