package com.sjh.demo.threadPool;

/**
 * @Author: sjh
 * @Date: 2023-09-02
 * @Version: 1.0
 */
public class ThreadPoolExecutor {
//    线程池的执行流程其实就是在说execute方法内部做了那些判断
//    线程池执行任务的一个逻辑

    //    提交任务到线程池的核心方法
//    command就是提交过来的任务
    public void execute(Runnable command) {
/*//        提交的任务不能为空
        if (command == null) {
            throw new NullPointerException();
        }
//        获取核心属性ctl，用于后面的判断
        int c = ctl.get();
//        如果工作线程个数，小于核心线程数；
//        满足要求，添加核心工程线程
        if (wokerCountOf(c) < corePoolSize) {
//            addWorker(任务，是核心系线程)
//            addWorker返回true：代表添加工作线程成功
//            addWorker返回false：代表添加工作线程失败
//            addWorker中会基于线程池状态，以及工作线程个数做判断，查看能否添加工作线程
            if (addWorker(command, true)) {
//                工程线程构建出来了，任务也交给了command去处理了

                return;
            }
//            说明线程池状态或者是工作线程个数发生了变化，导致添加失败，重新获取一次ctl
            c = ctl.get();
        }
//        添加核心工作线程失败，往这走
//        判断线程池状态是否是Running，如果是，正常基于阻塞队列的offer方法，将任务添加到阻塞队列
        if (isRunning(c) && workQueue.offer(command)) {
//            如果任务添加到阻塞队列，走if内部
//            如果任务在扔到阻塞队列之前，线程池状态突然改变了
//            重新获取ctl
            int recheck = ctl.get();
//            如果线程池的状态不是Running，将任务从阻塞队列中移除
            if (!isRunning(recheck) && remove(mannod)) {
//                并且直接拒绝策略
                reject(command);
            }
//            在这，说明阻塞队列有我刚刚放进去的任务
//            查看一下工作线程数是不是0个
//            如果工作线程为0个，需要添加一个非核心工作工作贤臣俄国去处理阻塞队列中的任务
//            发生这种情况有两种
//            1.构建线程池时，核心线程数是0个
//            2.即便有核心线程，可以设置核心线程也允许超时，设置allowCoreThreadTimeOut为true，
//            代表核心线程也可以
            else if (workerCountOf(recheck) == 0) {
//                为了避免阻塞队列中的任务饥饿，添加一个非核心工作线程去处理
                addWorker(null, false);
            }
//            任务添加到阻塞队列失败
//            构建一个非核心工作线程
//            如果添加非核心工作线程，直接完事
            else if (!addWorker(command, false)) {
//                添加失败，执行拒绝策略
                reject(command);
            }
        }*/

    }
}
