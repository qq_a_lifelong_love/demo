package com.sjh.demo.threadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: sjh
 * @Date: 2023-08-07
 * @Version: 1.0
 */
public class CachedThreadPoolTest {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        MyRunnable myRunnable = new MyRunnable();
        for (int i = 0; i < 5; i++) {
            executorService.execute(myRunnable);
        }
        System.out.println("线程任务开始执行了");
        executorService.shutdown();
    }
}
