package com.sjh.demo.threadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: sjh
 * @Date: 2023-08-07
 * @Version: 1.0
 */

/**
 * 从参数可以看出来，SingleThreadExecutor 相当于特殊的 FixedThreadPool，它的执行流程如下：
 *
 * 线程池中没有线程时，新建一个线程执行任务
 * 有一个线程以后，将任务加入阻塞队列，不停的加
 * 唯一的这一个线程不停地去队列里取任务执行
 * SingleThreadExecutor 用于串行执行任务的场景，每个任务必须按顺序执行，不需要并发执行。
 */
public class SingleThreadExecutorTest {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        MyRunnable myRunnable = new MyRunnable();
        for (int i = 0; i < 5; i++) {
            executorService.execute(myRunnable);
        }
        System.out.println("线程任务开始执行");
        executorService.shutdown();
    }
}
