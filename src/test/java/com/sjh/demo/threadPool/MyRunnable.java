package com.sjh.demo.threadPool;

/**
 * @Author: sjh
 * @Date: 2023-08-07
 * @Version: 1.0
 */
public class MyRunnable implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"is running...");
    }
}
