package com.sjh.demo.threadPool;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @Author: sjh
 * @Date: 2023-08-07
 * @Version: 1.0
 */

/**
 * ScheduledThreadPoolExecutor 的执行流程如下：
 *
 * 添加一个任务
 * 线程池中的线程从 DelayQueue 中取任务
 * 然后执行任务
 * 具体执行任务的步骤也比较复杂：
 *
 * 线程从 DelayQueue 中获取 time 大于等于当前时间的 ScheduledFutureTask
 *
 * 执行完后修改这个 task 的 time 为下次被执行的时间
 *
 * 然后再把这个 task 放回队列中
 *
 * ScheduledThreadPoolExecutor 用于需要多个后台线程执行周期任务，同时需要限制线程数量的场景。
 */
public class ScheduledThreadPoolTest {
    public static void main(String[] args) {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(3);
        MyRunnable myRunnable = new MyRunnable();
        for (int i = 0; i < 5; i++) {
//            参数1：目的对象，参数2：隔多长时间开始执行线程，参数3：执行周期，参数4：时间单位
            scheduledExecutorService.scheduleAtFixedRate(myRunnable,1,2, TimeUnit.SECONDS);

        }
        System.out.println("线程任务开始执行");
//        scheduledExecutorService.shutdown();
    }
}
