package com.sjh.demo;

/**
 * @Author: sjh
 * @Date: 2023-08-02
 * @Version: 1.0
 */
public class NumCheck {
    public static void main(String[] args) {
        exceed(2000);
        exceed(9000);
        exceed(9001);

        exceed(0);

        exceed(1111110111);


        exceed(Integer.MAX_VALUE);
    }

    //    商品最大限制
    public final static int LIMIT = 10000;

    //    检查是否可以预定
    static boolean exceed(int order) {
//    会员当前拥有的产品数量
        int cur = 1000;
        int totalNum = order + cur;
        if (order > 0 && totalNum > 0 && totalNum <= LIMIT) {
            System.out.println("你已经预定：" + order + "个产品");
        } else if (order <= 0) {
            System.out.println("经预产品个数必须大于0");
        } else {
            System.out.println("超过限额，预定失败");
        }
        return false;
    }
}
