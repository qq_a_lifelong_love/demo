package com.sjh.demo.java8;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-25
 * @Version: 1.0
 */
public class DateTimeAPI {


    /**
     * 新增了日期时间类
     * LocalDate： 表示日期类。yyyy-MM-dd
     * LocalTime: 表示时间类。 HH:mm:ss
     * LocalDateTime: 表示日期时间类 yyyy-MM-dd t HH:mm:ss sss
     * DatetimeFormatter:日期时间格式转换类。
     * Instant: 时间戳类。
     * Duration: 用于计算两个日期类
     * 原文链接：https://blog.csdn.net/Aimeiyyds/article/details/125879390
     */

    @Test
    public void test() {
//        获取当前日期
        LocalDate now = LocalDate.now();
        System.out.println(now);
//        指定日期
        LocalDate of = LocalDate.of(2024, 04, 25);
        System.out.println(of);

//        当前时间
        LocalDateTime now1 = LocalDateTime.now();
        System.out.println(now1);
        LocalDateTime of1 = LocalDateTime.of(2024, 04, 25, 14, 33,55,333);
        System.out.println(of1);
//        获取当前日期时间
        LocalDateTime now2 = LocalDateTime.now();
        System.out.println(now2);
        LocalDateTime now3 = LocalDateTime.of(2024, 04, 25, 15, 00, 45);
        Duration between = Duration.between(now2, now3);
        System.out.println(between.toHours());

        DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        //把字符串转换为日期时间格式
        LocalDateTime dateTime = LocalDateTime.parse("2024-04-25 15:06:50", dateTimeFormatter);
        System.out.println(dateTime);
//        把字符串转换成日期格式
        DateTimeFormatter dateTimeFormatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String s = LocalDate.now().toString();
        System.out.println(s);
        LocalDate parse = LocalDate.parse(LocalDate.now().toString(), dateTimeFormatter1);
        System.out.println(parse);
//        //把日期格式转换为字符串
//        String format = parse.format(dateTimeFormatter);
//        System.out.println(format);



    }
}
