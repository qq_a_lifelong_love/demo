package com.sjh.demo.java8;

import ch.qos.logback.core.joran.action.AppenderRefAction;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-23
 * @Version: 1.0
 */
public class StreamAPI {

    //创建 Stream方式一：通过集合
    @Test
    public void test1() {

        List<Product> list = Arrays.asList(
                new Product(1, 4, 7),
                new Product(2, 5, 8),
                new Product(3, 6, 9));
//        stream() : 返回一个顺序流
        Stream<Product> stream = list.stream();
        System.out.println(stream);

        List<Product> productList = list.stream().collect(Collectors.toList());
        productList.forEach(System.out::println);

        System.out.println(productList);

//        parallelStream()  默认返回一个并行流
        Stream<Product> productStream = list.parallelStream();
        System.out.println(productStream);
    }

    //    创建stream方式二：通过数组
    @Test
    public void test2() {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6};
//         //调用Arrays类的static <T> Stream<T> stream(T[] array): 返回一个流
        IntStream stream = Arrays.stream(arr);
        System.out.println(stream);

        Product product1 = new Product(1, 4, 7);
        Product product2 = new Product(2, 5, 8);
        Product product3 = new Product(3, 6, 9);
        Product[] arr2 = new Product[]{product1, product2, product3};
        Stream<Product> stream1 = Arrays.stream(arr2);
        System.out.println(stream1);
    }

    //创建 Stream方式三：通过Stream的of()
    @Test
    public void test3() {
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6);
        System.out.println(stream);
    }

    //创建 Stream方式四：创建无限流
    @Test
    public void test4() {

//      迭代
//      public static<T> Stream<T> iterate(final T seed, final UnaryOperator<T> f)
        //遍历前10个偶数
        Stream.iterate(0, t -> t + 2).limit(10).forEach(System.out::println);


//      生成
//      public static<T> Stream                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <T> generate(Supplier<T> s)
        Stream.generate(Math::random).limit(10).forEach(System.out::println);

    }


    //    ============Stream 的中间操作=========筛选与切片===================================
    @Test
    public void test5() {
        List<Product> productList = Arrays.asList(new Product(1, 4, 7),
                new Product(2, 5, 8),
                new Product(3, 6, 9));
        //        filter(Predicate p)——接收 Lambda ， 从流中排除某些元素。
        Stream<Product> stream = productList.stream();
//        stream.filter(p -> p.getProductId()==2).forEach(System.out::println);

        //        limit(n)——截断流，使其元素不超过给定数量。
//        productList.stream().limit(3).forEach(System.out::println);

        //        skip(n) —— 跳过元素，返回一个扔掉了前 n 个元素的流。若流中元素不足 n 个，则返回一个空流。与 limit(n) 互补
//        productList.stream().skip(1).forEach(System.out::println);

        //        distinct()——筛选，通过流所生成元素的 hashCode() 和 equals() 去除重复元素

        productList.stream().distinct().forEach(System.out::println);

    }
//    ===================Stream 的中间操作===========映射==============================================

    @Test
    public void test6() {
        //        map(Function f)——接收一个函数作为参数，将元素转换成其他形式或提取信息，该函数会被应用到每个元素上，并将其映射成一个新的元素。
        List<String> list = Arrays.asList("aa", "bb", "cc", "dd");
        list.stream().map(s -> s.toUpperCase()).forEach(System.out::println);


        List<Product> productList = Arrays.asList(new Product(1, 4, 7),
                new Product(2, 5, 8),
                new Product(3, 6, 9));
//        获取
        Stream<Double> priceStream = productList.stream().map(Product::getPrice);
        priceStream.filter(price -> price >= 5).forEach(System.out::println);
    }

    @Test
    public void test7() {
        ArrayList list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        ArrayList list2 = new ArrayList<>();
        list2.add(4);
        list2.add(5);
        list2.add(6);

        list1.addAll(list2);
        System.out.println(list1);
    }

    //    排序
    @Test
    public void test8() {
//        sortes()   ----自然排序
        List<Integer> list = Arrays.asList(12, 43, 65, 34, 87, 0, -98, 7, 7);
        list.stream().distinct().sorted().forEach(System.out::println);


        //        sorted(Comparator com)——定制排序
        List<Product> productList = Arrays.asList(new Product(1, 4, 74),
                new Product(2, 5, 8),
                new Product(3, 6, 9));
        productList.stream().sorted((p1, p2) -> {
            int num = Integer.compare(p1.getNum(), p2.getNum());
            if (num != 0) {
                return num;
            } else {
                return Double.compare(p1.getPrice(), p2.getPrice());
            }
        }).forEach(System.out::println);
    }

    //    =================Stream 的终止操作==========匹配与查找======
    @Test
    public void test9() {
        List<Product> productList = Arrays.asList(new Product(1, 4, 7, "爆米花"),
                new Product(2, 5, 8, "大香肠"),
                new Product(3, 6, 9, "糖葫芦"));
//        是否所有的数量都大于6
        boolean allMath = productList.stream().allMatch(product -> product.getNum() > 6);
        System.out.println(allMath);
//        是否存在price大于5
        boolean price = productList.stream().anyMatch(product -> product.getPrice() > 5);
        System.out.println(price);

//        是否存在"大香肠"
        boolean noneMatch = productList.stream().noneMatch(product -> product.getName().startsWith("大香肠"));
        System.out.println(noneMatch);

        //        findFirst——返回第一个元素
        Optional<Product> first = productList.stream().findFirst();
        System.out.println(first);

        //        findAny——返回当前流中的任意元素
        Optional<Product> any = productList.stream().findAny();
        System.out.println(any);

    }

    @Test
    public void test10() {
        List<Product> productList = Arrays.asList(new Product(1, 4, 7, "爆米花"),
                new Product(2, 5, 8, "大香肠"),
                new Product(3, 6, 9, "糖葫芦"));

//        // count——返回流中元素的总个数
        long count = productList.stream().count();
        System.out.println(count);
        long count1 = productList.stream().filter(product -> product.getNum() >= 8).count();
        System.out.println(count1);

//        max(Comparator c)——返回流中最大值
//        返回最高价格
        Optional<Double> max = productList.stream().map(product -> product.getPrice()).max(Double::compare);
        System.out.println(max);

//        min(Comparator c)——返回流中最小值
//        返回最低的价格
        Optional<Double> min = productList.stream().map(product -> product.getPrice()).min(Double::compare);
        System.out.println(min);

//         forEach(Consumer c)——内部迭代
        productList.stream().forEach(System.out::println);

//        使用集合的遍历操作
        productList.forEach(System.out::println);

    }

//    =================Stream 的终止操作==========归约======

    @Test
    public void test11() {
//        reduce(T identity, BinaryOperator)——可以将流中元素反复结合起来，得到一个值。返回 T
//        练习1：计算1-10的自然数的和
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Integer sum = list.stream().reduce(0, Integer::sum);
        System.out.println(sum);
        Optional<Integer> max = list.stream().reduce(Integer::max);
        System.out.println(max);

        Optional<Integer> min = list.stream().reduce(Integer::min);
        System.out.println(min);

//        reduce(BinaryOperator) ——可以将流中元素反复结合起来，得到一个值。返回 Optional<T>
//        计算所有价格的和
        List<Product> productList = Arrays.asList(new Product(1, 4, 7, "爆米花"),
                new Product(2, 5, 8, "大香肠"),
                new Product(3, 6, 9, "糖葫芦"));
        Optional<Double> sumPrice = productList.stream().map(product -> product.getPrice()).reduce((p1, p2) -> p1 + p2);
        System.out.println(sumPrice.get());

        Optional<Double> sum1 = productList.stream().map(product -> product.getPrice()).reduce(Double::sum);
        System.out.println(sum1);

    }

    //    //    =================Stream 的终止操作==========收集======
    @Test
    public void test12() {
//        collect(Collector c)——将流转换为其他形式。接收一个 Collector接口的实现，用于给Stream中元素做汇总的方法
//        练习1：查找价格大4的，结果返回为一个List或Set
        List<Product> productList = Arrays.asList(new Product(1, 4, 7, "爆米花"),
                new Product(2, 5, 8, "大香肠"),
                new Product(3, 6, 9, "糖葫芦"));
        List<Product> list = productList.stream().filter(product -> product.getPrice() > 4).collect(Collectors.toList());
        System.out.println(list);

        Set<Product> set = productList.stream().filter(product -> product.getPrice() > 4).collect(Collectors.toSet());
        System.out.println(set);
    }

    class Product {
        private int productId;
        private double price;
        private int num;
        private String name;

        public Product(int productId, double price, int num, String name) {
            this.productId = productId;
            this.price = price;
            this.num = num;
            this.name = name;
        }

        public Product(int productId, double price, int num) {
            this.productId = productId;
            this.price = price;
            this.num = num;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        //    toString方法进行打印


        @Override
        public String toString() {
            return "Product{" +
                    "productId=" + productId +
                    ", price=" + price +
                    ", num=" + num +
                    ", name='" + name + '\'' +
                    '}';
        }

        //    计算总价
        public double getTotalPrice() {
            return price * num;
        }
    }

    @Test
    public void test13() {
        List<String> list = new ArrayList<>();
        list.add("暧昧");
        list.add("小黄人");
        list.add("大宝贝");
        Stream<String> stream = list.stream();
        System.out.println(stream);

        Integer arr[] = {1, 3, 5, 7, 9};
        Stream<Integer> stream1 = Arrays.stream(arr);
        System.out.println(stream1);
        Stream.of("张三", "李四", "王五", "赵六");
//        上边都是串行流，这个是并行流，如果流中的数据量足够大，并行流可以加快速度
        Stream<String> stringStream = list.parallelStream();
        System.out.println(stringStream);
    }

    @Test
    public void test14(){
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("欧阳雪",18,"中国",'F'));
        personList.add(new Person("Tom",24,"美国",'M'));
        personList.add(new Person("Harley",22,"英国",'F'));
        personList.add(new Person("向天笑",20,"中国",'M'));
        personList.add(new Person("李康",22,"中国",'M'));
        personList.add(new Person("小梅",20,"中国",'F'));
        personList.add(new Person("何雪",21,"中国",'F'));
        personList.add(new Person("李康",22,"中国",'M'));

        //输出country为英国的元素
//        personList.stream().filter(item -> item.getCountry().equals("英国")).forEach(System.out::println);

        //输出sex为F的元素
        //personList.stream().filter(item->item.getSex()=='F').forEach(System.out::println);

        //只要name 和 age , 原来流中每个元素转换成另一种格式。
        // map--接收Lambda，将元素转换成其他形式或提取信息。接收一个函数作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元素。
//        personList.stream().map(item->{
//            Map<String,Object> map = new HashMap<>();
//            map.put("name",item.getName());
//            map.put("age",item.getAge());
//            return map;
//        }).forEach(System.out::println);

        //根据年龄排序
//        personList.stream().sorted((p1,p2)->p1.getAge()-p2.getAge()).forEach(System.out::println);

        //输出年龄最大的
        //Optional<Person> max = personList.stream().max((o1, o2) -> o1.getAge() - o2.getAge());
        //System.out.println(max.get());

        //输出年龄最小的
        //Optional<Person> min = personList.stream().min((o1, o2) -> o1.getAge() - o2.getAge());
        //System.out.println(min);

        //规约reduce
        //求集合中所有人的年龄和
        //Optional<Integer> reduce = personList.stream().map(item -> item.getAge()).reduce((a, b) -> a + b);
        //System.out.println(reduce.get());

        //求a有初始化值的规约
        //Integer reduce = personList.stream().map(item -> item.getAge()).reduce(20, (a, b) -> a + b);
        //System.out.println(reduce);

        //collect搜集  搜集年龄大于20且性别为F的
        //List<Person> collect = personList.stream().filter(item -> item.getAge() > 20).filter(item -> item.getSex() == 'F').collect(Collectors.toList());

        //findFirst() 方法根据命名可以大致知道是获取Optional流中的第一个元素。
        //findAny() 方法是获取Optional 流中任意一个，存在随机性，其实里面也是获取元素中的第一个。(适用于并行流)
        //Optional<Person> first = personList.stream().filter(item -> item.getSex() == 'F').findFirst();
        //System.out.println(first.get());

        //allMatch  查询所有元素都符合条件则返回true
        //boolean b = personList.stream().allMatch(item -> item.getAge() > 17);
        //System.out.println(b);

        //anyMatch  查询任一元素有一个符合条件则返回true
        //boolean b = personList.stream().anyMatch(item -> item.getAge() > 23);
        //System.out.println(b);

        //noneMatch 查询没有一个元素符合我们的条件则返回true
        //boolean b = personList.stream().noneMatch(item -> item.getAge() > 23);
        //System.out.println(b);
    }
}
class Person {
    private String name;
    private Integer age;
    private String country;
    private char sex;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", country='" + country + '\'' +
                ", sex=" + sex +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public Person() {
    }

    public Person(String name, Integer age, String country, char sex) {
        this.name = name;
        this.age = age;
        this.country = country;
        this.sex = sex;
    }

}
