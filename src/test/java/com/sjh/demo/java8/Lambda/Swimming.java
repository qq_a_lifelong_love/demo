package com.sjh.demo.java8.Lambda;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-24
 * @Version: 1.0
 */
public interface Swimming {

    // 游泳方法
    void swimm();
}
