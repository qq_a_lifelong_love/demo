package com.sjh.demo.java8.Lambda;

import org.junit.jupiter.api.Test;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-24
 * @Version: 1.0
 */
public class Lambda1 {

    @Test
    public void test1() {
        // 1、调用方法传递匿名内部类对象
        // 面向对象编程风格
        goSwimm(new Swimming() { //多态:编译看左边（父类），运行看右边(子类)
            @Override
            public void swimm() {
                System.out.println("小明在游泳...");
            }
        });

        System.out.println("----------------------------");
        // 2、使用lambda表达式简化上面的匿名内部类（接口中只提供一个抽象方法）
        // 调用方法传递lambda表达式
        goSwimm(() -> {
            System.out.println("王五在游泳...");
        });

    }


    /**
     * 定义一个静态方法,使用Swimming接口作为方法参数,然后调用的时候我们只需传递接口实现类对象便可执行具体方法
     *
     * @param swimm 匿名内部类对象、接口实现类对象
     */
    public static void goSwimm(Swimming swimm) {
        swimm.swimm();
    }


}
