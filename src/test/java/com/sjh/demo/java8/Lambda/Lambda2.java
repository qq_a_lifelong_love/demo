package com.sjh.demo.java8.Lambda;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-24
 * @Version: 1.0
 */
public class Lambda2 {
    public static void main(String[] args) {

        // 1.传统方式 需要new接口的实现类来完成对接口的调用
        ICar car1 = new IcarImpl();
        car1.drive();

        // 2.匿名内部类使用
        ICar car2 = new ICar() {
            @Override
            public void drive() {
                System.out.println("Drive BMW");
            }
        };
        car2.drive();

        // 3.无参无返回Lambda表达式
        ICar car3 = () -> {
            System.out.println("Drive Audi");
        };
        car3.drive();

        // 4.无参无返回且只有一行实现时可以去掉{}让Lambda更简洁
        ICar car4 = () -> System.out.println("Drive Ferrari");
        car4.drive();

        // 去查看编译后的class文件 大家可以发现 使用传统方式或匿名内部类都会生成额外的class文件，而Lambda不会
    }

}

interface ICar {

    void drive();
}

class IcarImpl implements ICar {

    @Override
    public void drive() {
        System.out.println("Drive Benz");
    }

}
