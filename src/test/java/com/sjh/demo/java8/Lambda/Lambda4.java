package com.sjh.demo.java8.Lambda;

import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

/**
 * @Author: 宋军辉
 * @Date: 2024-04-24
 * @Version: 1.0
 */
public class Lambda4 {
//    . lambda表达式
    @Test
    public void test1() {
        Runnable runnable = () -> {
            System.out.println("这是lamdba表达式方式创建的任务方法对象");
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

//    2. 函数式接口:一个接口只有一个抽象方法，则该接口称之为函数式接口
//    函数式接口可以使用Lambda表达式，Lambda表达式会被匹配到这个抽象方法上。
    @Test
    public void test2(){
        fun(arr -> {
            int sum=0;
            for(int n:arr){
                sum+=n;
            }
            System.out.println("数组的和为:"+sum);
        });
    }

    public static void fun(Consumer<int[]> consumer){
        int[] arr={1,3,5,7,8,10};
        consumer.accept(arr);
    }



}
