package com.sjh.demo.reverse;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Author: 宋军辉
 * @Date: 2024-05-08
 * @Version: 1.0
 */

public class ReverseNumber {

    /**
     * 数字反转，比如:123456 转换完数字是 654321
     */
    @Test
    public void test01() {
        int num = 123456;
        int reversed = 0;

        while (num != 0) {
            int digit = num % 10;
            reversed = reversed * 10 + digit;
            num /= 10;
        }
        System.out.println("反转后: " + reversed);
    }

    /**
     * 数字反转，比如:123456 转换完数字是 654321
     */
    @Test
    public void test02() {
        int num = 123456;
        int reversed = 0;

        for (; num != 0; num /= 10) {
            int digit = num % 10;
            reversed = reversed * 10 + digit;
        }

        System.out.println("Reversed Number: " + reversed);
    }
}
