package com.sjh.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;


/**
 * @Author: sjh
 * @Date: 2023-08-04
 * @Version: 1.0
 */

@SpringBootTest
public class AsList {


    public static void main(String[] args) {
        List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
//    求和
        int sum = 0;
        for (Integer prime : primes) {
            sum += prime;
        }
        System.out.println("sum =" + sum);

//        求平均数
        double average = (double) sum / primes.size();
        System.out.println("average = " + average);

//        找出最大值
        int max = Integer.MIN_VALUE;
        for (Integer prime : primes) {
            if (prime > max) {
                max = prime;
            }
        }
        System.out.println("max = " + max);

//        找出最小值
        int min = Integer.MAX_VALUE;
        for (Integer prime : primes) {
            if (prime < min) {
                min = prime;
            }
        }
        System.out.println("min =" + min);

//        统计数量
        int count = primes.size();
        System.out.println("count = " + count);
    }
}
