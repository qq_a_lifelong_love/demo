package com.sjh.demo.shuiwu;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: 宋军辉
 * @Date: 2024-05-12
 * @Version: 1.0
 */
public class Sw2 {


    public static void main(String[] args) {
//        1 准备好数据，准备8条TaxSection对象，放到一个集合里面，list
        List<TaxSection> taxSectionList = new ArrayList<>();
        taxSectionList.add(new TaxSection(0, 5000, 0.0));
        taxSectionList.add(new TaxSection(5000, 8000, 0.03));
        taxSectionList.add(new TaxSection(8000, 17000, 0.1));
        taxSectionList.add(new TaxSection(17000, 30000, 0.2));
        taxSectionList.add(new TaxSection(30000, 40000, 0.25));
        taxSectionList.add(new TaxSection(40000, 60000, 0.3));
        taxSectionList.add(new TaxSection(60000, 85000, 0.35));
        taxSectionList.add(new TaxSection(85000, Double.MAX_VALUE, 0.45));
//        2遍历list集合，分别调用TaxSection对象的calTax方法

//        3把八个方法的返回结果求和就是要交的税费
        double result = 0;
        for (TaxSection taxSection:taxSectionList){
            result+=taxSection.calTax(8000);
        }
//        或者用jdk8的特性
        double result1=taxSectionList.stream().mapToDouble(taxSection->taxSection.calTax(8000)).sum();

        System.out.println(result1);
    }


}
