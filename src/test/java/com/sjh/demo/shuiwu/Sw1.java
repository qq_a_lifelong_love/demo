package com.sjh.demo.shuiwu;


import org.junit.jupiter.api.Test;

import java.util.Scanner;

/**
 * @Author: 宋军辉
 * @Date: 2024-05-12
 * @Version: 1.0
 */
public class Sw1 {

    /**
     * 计算个人所得税
     * 根据用户输入的薪水计算个人所得税并打印出来，其中个税起征点为：5000元，具体规则如下：
     * <p>
     * 个人所得税起征点每月5000元。个人所得税起征点为5000元/月或60000万元/年，工资范围以及税率：
     * <p>
     * 1、工资范围在1至5000元之间的，包括5000元，适用个人所得税税率为百分之零；
     * 2、工资范围在5000至8000元之间的，包括8000元，适用个人所得税税率为百分之三；
     * 3、工资范围在8000至17000元之间的，包括17000元，适用个人所得税税率为百分之十；
     * 4、工资范围在17000至30000元之间的，包括30000元，适用个人所得税税率为百分之二十；
     * 5、工资范围在30000至40000元之间的，包括40000元，适用个人所得税税率为百分之二十五；
     * 6、工资范围在40000至60000元之间的，包括60000元，适用个人所得税税率为百分之三十；
     * 7、工资范围在60000至85000元之间的，包括85000元，适用个人所得税税率为百分之三十五；
     * 8、工资范围在85000元以上的，适用个人所得税税率为百分之四十五。
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入税前工资:");
        double total = scanner.nextDouble();
        double taxIncome = total - 5000;
        double tax = 0.0;
        if (total < 5000) {
            tax = 0.0;
        } else if (taxIncome < 8000) {
            tax = taxIncome * 3/100;
        } else if (total < 17000) {
            tax = taxIncome * 0.1 - 105;
        } else if (taxIncome <= 30000) {
            tax = taxIncome * 20 / 100 - 555;
        } else if (taxIncome <= 40000) {
            tax = taxIncome * 25 / 100 - 1005;
        } else if (taxIncome <= 60000) {
            tax = taxIncome * 30 / 100 - 2755;
        } else if (taxIncome <= 85000) {
            tax = taxIncome * 35 / 100 - 5505;
        } else {
            tax = taxIncome * 45 / 100 - 13505;
        }
        System.out.println("您应缴纳：" + tax + "元个人所得税");
    }

    @Test
    public void test01() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入税前工资:");
        double total = scanner.nextDouble();
        double taxIncome = total - 5000;
        double tax = 0.0;
        if (total < 5000) {
            tax = 0.0;
        } else if (taxIncome < 8000) {
            tax = taxIncome * 0.03;
        } else if (total < 17000) {
            tax = taxIncome * 0.1 - 105;
        } else if (taxIncome <= 30000) {
            tax = taxIncome * 20 / 100 - 555;
        } else if (taxIncome <= 40000) {
            tax = taxIncome * 25 / 100 - 1005;
        } else if (taxIncome <= 60000) {
            tax = taxIncome * 30 / 100 - 2755;
        } else if (taxIncome <= 85000) {
            tax = taxIncome * 35 / 100 - 5505;
        } else {
            tax = taxIncome * 45 / 100 - 13505;
        }
        System.out.println("您应缴纳：" + tax + "元个人所得税");

    }
}
