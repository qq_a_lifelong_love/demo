package com.sjh.demo.shuiwu;

/**
 * @Author: 宋军辉
 * @Date: 2024-05-12
 * @Version: 1.0
 */
public class TaxSection {
    /**
     * 封装每个容易变化的税率信息
     */
//    薪资的最低边界
    private double salStart;
    //    薪资的最高边界
    private double salEnd;
    //    税率
    private double tax;

    public TaxSection() {
    }

    public TaxSection(double salStart, double salEnd, double tax) {
        this.salStart = salStart;
        this.salEnd = salEnd;
        this.tax = tax;
    }

    //    根据薪资计算该对象范围内的税费
    public double calTax(double salary) {
        if (salary <= salStart) {
            return 0.0;
        }
        double taxCollectedAmount = salary > salEnd ? salEnd - salStart : salary - salStart;
        return taxCollectedAmount * tax;
    }
}
