package com.sjh.demo;

import com.google.common.base.CharMatcher;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

//import com.google.common.base.CharMatcher;

/**
 * @Author: sjh
 * @Date: 2023-08-04
 * @Version: 1.0
 */
@SpringBootTest
public class Filter {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("1", "zs");
        map.put("2", "ls");
        map.put("3", "ww");
        map.put("4", "zl");
        //基于JDK1.8版本以上 lambada函数
        map.forEach((k, v) -> System.out.println("编号：" + k + " 姓名：" + v));
        //keySet获取map集合key的集合 然后在遍历key即可
        for (String key : map.keySet()) {
            String value = map.get(key).toString();
            System.out.println("编号：" + key + " 姓名：" + value);
        }
        //Map集合循环遍历方式三 推荐，尤其是容量大时
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            System.out.println("key:" + entry.getKey() + " value:  " + entry.getValue());
        }
        // Map集合循环遍历方式三 推荐，尤其是容量大时
        for (Map.Entry<String, String> m : map.entrySet()) {
            System.out.println("key : " + m.getKey() + "value : " + m.getValue());
        }


        //通过Map.values()遍历所有的value，但不能遍历key
        for (Object m : map.values()) {
            System.out.println("value :" + m);

        }


        List<String> list = new ArrayList<>();
//        程序员代码时间占比
        list.add("看需求->10%");
        list.add("论需求->50%");
        list.add("改需求->40%");
        list.add("写代码->10%");
        list.add("看代码->70%");
        list.add("测代码->20%");

//        Filter containing '代码'
        List<String> filteredList = list.stream()//分段操作
                .filter(stage -> stage.contains("代码"))//过滤操作
                .collect(Collectors.toList());//
        filteredList.forEach(System.out::println);


//        Filter containing '代码'
//        List<String> filteredList = new ArrayList<>();
//        for (String stage : list) {
//            if (stage.contains("代码")){
//                filteredList.add(stage);
//            }
//        }

        //        Sort filtered based on '->' delimiter
        List<String> sortList = filteredList.stream()//分段操作
                .sorted(Comparator.comparingInt(stage -> {
                    String s = CharMatcher.inRange('0', '9').retainFrom(stage);
                    return Integer.parseInt(s);
                })).collect(Collectors.toList());
//        Sort filtered based on '->' delimiter
//        filteredList.sort(new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                int arrowIndex1 = o1.indexOf("->");
//                int arrowIndex2 = o2.indexOf("->");
//                String percentage1 = o1.substring(arrowIndex1 + 2, o1.length() - 1);
//                String percentage2 = o1.substring(arrowIndex2 + 2, o2.length() - 1);
//                return Integer.parseInt(percentage1)-Integer.parseInt(percentage2);
//            }
//        });
        //        Print results
        sortList.forEach(System.out::println);

//        Print results
//        for (String stage : filteredList) {
//            System.out.println(stage);
//        }
        LocalDateTime now = LocalDateTime.now();
        System.out.println("当前时间：" + now);
        if (now.getHour() > 17) {
            LocalDateTime dateTime = now.plusDays(1);
            System.out.println("当前时间加一天：" + dateTime);
        }
    }

    @Test
    public void test01() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("当前时间：" + now);
        if (now.getHour() > 17) {
            LocalDateTime dateTime = now.plusDays(1);
            System.out.println("当前时间加一天：" + dateTime);
        }
    }
}
