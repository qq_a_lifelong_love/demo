package com.sjh.demo.map;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: sjh
 * @Date: 2023-09-22
 * @Version: 1.0
 */
@SpringBootTest
public class TestMap {
    /**
     * 输出
     * ref1:{key2=value2}
     * ref2:{key1=value1}
     * 证明 ref2=ref1,实际上是将ref1指向的map地址赋值给了ref2，当ref1=new Map()指向新的地址时间，不变
     *
     * @param args
     */

    public static void main(String[] args) {
        Map<String, String> ref1 = null, ref2 = null;
        ref1 = new HashMap<>();
        ref1.put("key1", "value1");

        ref2 = ref1;

        ref1 = new HashMap<>();
        ref1.put("key2", "value2");
        System.out.println("ref1:" + ref1);
        System.out.println("ref2:" + ref2);
    }

}
